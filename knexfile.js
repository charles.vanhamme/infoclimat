const config = require('config');

module.exports = {
  client: config.get('db.client'),
  connection: config.get('db.connection'),
  migrations: {
    directory: './src/db/migrations',
    tableName: 'knex_migrations',
  },
  seeds: {
    directory: './src/db/seeds',
  },
};
