# Shooterz - GraphQL API

The API allows back-office and mobile application to interact with PGSQL Database.

## Installation

Install dependencies & chill while it is installing

```bash
npm install
```

Up **PGSQL** container

```bash
docker-compose up -d
```

Populate database

```bash
npm run seed
```

API should be ready on **http://localhost:4000/**
