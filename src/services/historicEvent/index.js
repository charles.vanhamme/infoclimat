import HistoricEvent from '../../db/models/HistoricEvent';

const prepareQuery = ({ filters }) =>
  HistoricEvent.query().modify('filters', { filters });

class HistoricEventService {
  static async findById(id) {
    return HistoricEvent.query().findById(id);
  }
  static async getCount({ filters }) {
    return prepareQuery({
      filters,
    })
      .count('historic_events.id as total')
      .then((r) => r[0].total);
  }

  static async queryRange({ cursor, filters, limit }) {
    const results = await HistoricEvent.query()
      .from(prepareQuery({ filters }).as('historic_events_filtered'))
      .orderBy('date_deb', 'asc')
      .offset(cursor * limit)
      .limit(limit);

    const total = await this.getCount({ filters });

    const isEnd = total <= limit;
    return {
      historicEvents: results,
      nextCursor: isEnd ? null : cursor + 1,
      total,
    };
  }
}

export default HistoricEventService;
