import HistoricValue from '../../db/models/HistoricValue';

class HistoricValueService {
  static async findById(id) {
    return HistoricValue.relatedQuery('historicValues').for(id);
  }

  static async getValuesByDepartment({ idHistoric, departmentCode }) {
    return HistoricValue.query().where({
      idHistoric,
      dept: departmentCode,
    }).orderBy('date', 'desc');
  }
}

export default HistoricValueService;
