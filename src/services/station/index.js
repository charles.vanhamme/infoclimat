import Station from '../../db/models/Station';

const prepareQuery = ({ filters }) =>
  Station.query().modify('filters', { filters });

class StationService {
  static async findById(id) {
    return Station.query().findById(id);
  }
  static async getCount({ filters }) {
    return prepareQuery({
      filters,
    })
      .countDistinct('stations.uniqueid')
      .then((r) => (r[0] ? r[0].count : 0));
  }

  static async queryRange({ cursor, filters, limit, orderBy }) {
    const orderModifier = (queryBuilder) => {
      switch (orderBy) {
        case 'AZ':
        default:
          queryBuilder.orderBy('libelle', 'asc');
          break;
        case 'ZA':
          queryBuilder.orderBy('libelle', 'desc');
          break;
      }
    };

    const results = await Station.query()
      .from(
        prepareQuery({ filters })
          .distinctOn('stations.uniqueid')
          .as('stations_filtered')
      )
      .offset(cursor)
      .limit(limit)
      .modify(orderModifier);
    const total = await this.getCount({ filters });

    const isEnd = total <= cursor + results.length;
    return {
      stations: results,
      nextCursor: isEnd ? null : cursor + results.length,
      total,
    };
  }
}

export default StationService;
