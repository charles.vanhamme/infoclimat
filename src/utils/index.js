import config from 'config';

import s3 from './s3Bucket.js';

const fileType = require('file-type');

const getB64FileInfo = async (b64) => {
  const b64File = new Buffer.from(
    b64.replace(/^data:image\/\w+;base64,/, ''),
    'base64'
  );
  const { ext, type } = await fileType.fromBuffer(b64File);
  return { b64File, type, ext };
};

const createObjectParams = (eventId, b64File, type, ext) => ({
  Bucket: config.get('awsS3.bucket.name'),
  Key: `${eventId}/media_${new Date().getTime()}.${ext}`,
  Body: b64File,
  ContentEncoding: config.get('awsS3.bucket.contendEncoding'),
  ContentType: `${type}/${ext}`,
});

const uploadOnBucket = async (objectParams) =>
  new Promise((resolve, reject) => {
    s3.upload(objectParams, function (err, data) {
      if (err) reject(err);
      resolve(data);
    });
  });

const uploadFileFromB64 = async (b64String, eventId) => {
  const { b64File, type, ext } = await getB64FileInfo(b64String);
  const objectParams = createObjectParams(eventId, b64File, type, ext);
  return uploadOnBucket(objectParams);
};

export default uploadFileFromB64;
