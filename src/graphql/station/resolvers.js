import StationService from '../../services/station';

export default {
  Query: {
    getStationById: async (parent, { id }) => StationService.findById(id),
    getStations: (parent, { cursor, filters, limit, orderBy }) =>
      StationService.queryRange({ cursor, filters, limit, orderBy }),
  },
};
