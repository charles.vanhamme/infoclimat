
import HistoricValuesService from '../../services/historicValue';

export default {
  Query: {
    getHistoricValuesByDepartment: async (parent, { idHistoric, departmentCode }) => HistoricValuesService.getValuesByDepartment({
        idHistoric,
        departmentCode,
    }),
  },
};
