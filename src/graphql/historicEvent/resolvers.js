import HistoricEvent from '../../db/models/HistoricEvent';
import HistoricEventService from '../../services/historicEvent';

export default {
  Query: {
    getHistoricEventById: async (parent, { id }) =>
      HistoricEventService.findById(id),
    getHistoricEvents: (parent, { cursor, filters, limit, orderBy }) =>
      HistoricEventService.queryRange({ cursor, filters, limit, orderBy }),
  },
  HistoricEvent: {
    historicValues: ({ id }) =>
      HistoricEvent.relatedQuery('historicValues').for(id),
    historicCountValues: ({ id }) =>
      HistoricEvent.relatedQuery('historicValues')
        .for(id)
        .count('historic_values.id as total')
        .then((r) => r[0].total),
    historicMaxValues: ({ id }) =>
      HistoricEvent.relatedQuery('historicValues')
        .for(id)
        .max('historic_values.valeur as max')
        .then((r) => r[0].max),
    historicMinValues: ({ id }) =>
      HistoricEvent.relatedQuery('historicValues')
        .for(id)
        .min('historic_values.valeur as min')
        .then((r) => r[0].min),
    historicAvgValues: ({ id }) =>
      HistoricEvent.relatedQuery('historicValues')
        .for(id)
        .avg('historic_values.valeur as avg')
        .then((r) => r[0].avg),
  },
};
