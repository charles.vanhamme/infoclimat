import { ApolloError, ApolloServer, AuthenticationError } from 'apollo-server';
import config from 'config';

import { resolvers, typeDefs } from '../../graphql';
import logger from '../logger';

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: async () => {
    // const me = await getUserFromHeader(req);
    // return { me };
  },
  formatError: (error) => {
    if (
      error.originalError instanceof ApolloError ||
      error instanceof AuthenticationError
    ) {
      return error;
    }
    logger.error(error);
    return new Error('Internal server error');
  },
});

const port = config.get('server.port');

const startApolloServer = () => {
  server.listen({ port }).then(({ url }) => {
    logger.info(`🚀  Server ready at ${url}`);
  });
};

export { server };
export default startApolloServer;
