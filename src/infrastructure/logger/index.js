import winston, { format } from 'winston';

import consoleTransport from './console-transport';

const TRANSPORTS = [consoleTransport];

const logger = winston.createLogger({
  format: format.combine(format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' })),
  transports: TRANSPORTS,
  exitOnError: false,
});

export default logger;
