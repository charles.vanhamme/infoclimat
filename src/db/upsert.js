const upsert = async (params) => {
  const { knex, table, data, constraint } = params;
  if (Array.isArray(data)) {
    const result = [];
    for (const item of data) {
      const insert = knex(table).insert(item);
      const update = knex.queryBuilder().update(item);
      const row = await knex
        .raw(`? ON CONFLICT ${constraint} DO ? returning *`, [insert, update])
        .then((r) => r.rows[0]);
      result.push(row);
    }
    return result;
  }
  const insert = knex(table).insert(data);
  const update = knex.queryBuilder().update(data);
  return knex
    .raw(`? ON CONFLICT ${constraint} DO ? returning *`, [insert, update])
    .then((r) => r.rows[0]);
};

// this file is used in seeds where Babel is not available
module.exports = upsert;
