/* eslint-disable global-require */
import { Model } from 'objection';

import knex from '../index.js';

Model.knex(knex);

class Station extends Model {
  static get tableName() {
    return 'stations';
  }

  $beforeInsert() {
    this.createdAt = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updatedAt = new Date().toISOString();
  }
}

export default Station;
