/* eslint-disable global-require */
import { Model } from 'objection';

import knex from '../index.js';

Model.knex(knex);

class HistoricValue extends Model {
  static get tableName() {
    return 'historic_values';
  }

  $beforeInsert() {
    this.createdAt = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updatedAt = new Date().toISOString();
  }

  static get relationMappings() {
    const HistoricNormale = require('./HistoricNormale').default;
    return {
      historicNormale: {
        relation: Model.HasManyRelation,
        modelClass: HistoricNormale,
        join: {
          from: 'historic_values.geoid',
          to: 'historic_normales.geoid',
        },
      },
    };
  }
}

export default HistoricValue;
