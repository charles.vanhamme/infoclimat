/* eslint-disable global-require */
import { Model } from 'objection';

import knex from '../index.js';

Model.knex(knex);

class HistoricEvent extends Model {
  static get tableName() {
    return 'historic_events';
  }

  $beforeInsert() {
    this.createdAt = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updatedAt = new Date().toISOString();
  }

  static get relationMappings() {
    const HistoricValues = require('./HistoricValue').default;
    return {
      historicValues: {
        relation: Model.HasManyRelation,
        modelClass: HistoricValues,
        join: {
          from: 'historic_events.id',
          to: 'historic_values.id_historic',
        },
      },
    };
  }

  static get modifiers() {
    return {
      filters(query, { filters }) {
        if (!filters) return;
        const { type, duree, localisation, importance, dateDeb, dateFin, annee } =
          filters;
        if (type) query.where('type', 'like', `%${type}%`);
        if (duree) query.where('duree', duree);
        if (localisation)
          query.where('localisation', 'like', `%${localisation}%`);
        if (importance) query.where('importance', importance);
        if (dateDeb && dateFin) {
          query.where('dateDeb', '>=', dateDeb);
          query.where('dateFin', '<=', dateFin);
        }
        if (dateDeb && !dateFin) {
          query.where('dateDeb', '>=', dateDeb);
        }
        if (!dateDeb && dateFin) {
          query.where('dateFin', '<=', dateFin);
        }
        if (annee) {
          query.whereRaw(`YEAR(date_deb) = ${annee}`);
        }
      },
    };
  }
}

export default HistoricEvent;
