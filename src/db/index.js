import config from 'config';
import knex from 'knex';
import { knexSnakeCaseMappers } from 'objection';

const instance = knex({
  client: config.get('db.client'),
  connection: config.get('db.connection'),
  ...knexSnakeCaseMappers({
    underscoreBeforeDigits: true,
  }),
});

export default instance;
